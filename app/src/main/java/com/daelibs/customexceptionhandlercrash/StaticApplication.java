package com.daelibs.customexceptionhandlercrash;

import android.app.Application;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StaticApplication extends Application implements Thread.UncaughtExceptionHandler {
    public static final String TAG = "StaticApplication";

    private static final Thread.UncaughtExceptionHandler originalDefaultHandler = Thread.getDefaultUncaughtExceptionHandler();
    private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    private Handler handler;

    @Override
    public void onCreate() {
        super.onCreate();

        Log.i(TAG, "onCreate");
        Thread.setDefaultUncaughtExceptionHandler(this);

        handler = new Handler();
    }

    @Override
    public void uncaughtException(Thread thread, Throwable ex) {
        Log.e(TAG, "Caught Exception");

        // Something happens
        File sdCard = Environment.getExternalStorageDirectory();

        if (sdCard.canWrite()) {
            String filePath = sdCard.getAbsolutePath() + "/CustomEx.log";
            try {
                FileWriter fw = new FileWriter(filePath, true); //append
                PrintWriter pw = new PrintWriter(fw);
                try {
                    pw.print(dateFormat.format(new Date()));
                    pw.print(": ");
                    ex.printStackTrace(pw);
                } finally {
                    pw.close();
                    fw.close();
                }
            } catch (IOException ioEx) {
                Log.e(TAG, "IOException in global exception handler");
            }
        }
        Log.i(TAG, "Send to default exception handler");
        originalDefaultHandler.uncaughtException(thread, ex);
    }


}
